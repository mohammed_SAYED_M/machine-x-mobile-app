
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngJustGage'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
   $ionicConfigProvider.tabs.position('bottom');

  $stateProvider

    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'

  })

    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
  
  .state('tab.machines', {
      url: '/machines',
      views: {
        'tab-machines': {
          templateUrl: 'templates/tab-machines.html',
          controller: 'MachinesCtrl'
        }
      }
    })
    .state('tab.machine-detail', {
      url: '/machines/:machineId',
      views: {
        'tab-machines': {
          templateUrl: 'templates/machine-detail.html',
          controller: 'MachineDetailCtrl'
        }
      }
    })
    .state('tab.machine-settings', {
      url: '/settings/:machineId',
      views: {
        'tab-machines': {
          templateUrl: 'templates/machine-settings.html',
          controller: 'MachineSettingsCtrl'
        }
      }
    })
  .state('tab.addmachine', {
      url: '/addmachine',
      views: {
        'tab-machines': {
          templateUrl: 'templates/addmachine.html',
          controller: 'AddMachineCtrl'
        }
      }
    })
  .state('notifications', {
      url: '/notifications',
      templateUrl: 'templates/notifications.html',
      controller: 'NotificationsCtrl'
    })
  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});

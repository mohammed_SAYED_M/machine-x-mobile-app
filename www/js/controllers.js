angular.module('starter.controllers', [])
.controller('LoginCtrl', function($scope, $state) {
    $scope.data = {};
 
    $scope.login = function() {
        $state.go('tab.machines');
    }
})
.controller('MachinesCtrl', function($scope,$state, Machines) {
  $scope.notifications = function() {
    $state.go('notifications');
  };
  $scope.machines = Machines.all();
  $scope.remove = function(machine) {
    Machines.remove(machine);
  };
})

.controller('MachineDetailCtrl', function($scope, $stateParams,$state, Machines) {
  $scope.machine = Machines.get($stateParams.machineId);
   $scope.value1 = 42;
    setInterval(function(){
        $scope.$apply(function() {
            $scope.value1 = getRandomInt(10, 400);
        });
    }, 3000);

  $scope.settings = function() {
    $state.go('tab.machine-settings', {machineId : $stateParams.machineId });
  };
})
.controller('MachineSettingsCtrl', function($scope, $stateParams,$state,  Machines) {
  $scope.machine = Machines.get($stateParams.machineId);
 
})
.controller('AddMachineCtrl', function($scope,Machines,$state,$ionicHistory ) {
  $scope.data={};
  $scope.add = function() {
    Machines.add($scope.data);
    $ionicHistory.nextViewOptions({
    disableBack: true
    });
    $state.go('tab.machines');
  };

})
.controller('NotificationsCtrl', function($scope,$state,$ionicHistory) {
    $scope.myGoBack = function() {

    $ionicHistory.nextViewOptions({
    disableBack: true
    });
    $state.go('tab.machines');
  };
})
.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});

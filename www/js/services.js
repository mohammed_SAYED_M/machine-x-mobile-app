angular.module('starter.services', [])

.factory('Machines', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var machines = [{
    id: 0,
    name: 'Verdena Machine',
    type: 'Type A'
  }, {
    id: 1,
    name: 'X34',
    type: 'Type A'
  }, {
    id: 2,
    name: 'Zireen',
    type: 'Type A'
  }, {
    id: 3,
    name: 'Ottawa',
    type: 'Type B'
  }, {
    id: 4,
    name: 'YXena',
    type: 'Type B'
  }];

  return {
    all: function() {
      return machines;
    },
    remove: function(machine) {
      machines.splice(machines.indexOf(machine), 1);
    },
     add: function(machine) {
      machines.push((machine));
    },
    get: function(machineId) {
      for (var i = 0; i < machines.length; i++) {
        if (machines[i].id === parseInt(machineId)) {
          return machines[i];
        }
      }
      return null;
    }
  };
});
